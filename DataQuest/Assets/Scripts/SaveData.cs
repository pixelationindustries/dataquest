﻿using UnityEngine;

public class SaveData : MonoBehaviour {
    public Vector3 playerLocation;
    public string playerScene;

    public bool beatBoss1;
    public bool beatBoss2;
    public bool beatBoss3;
    public bool beatBoss4;
}
