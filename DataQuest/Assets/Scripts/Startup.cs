﻿using UnityEngine;
using System.Collections;

public class Startup : MonoBehaviour {

    public GameObject Savedata;

    void Start() {

        if (GameObject.Find("Savedata(Clone)") == null) {
            Instantiate(Savedata);
        }
        else {
            GameObject.Find("Player").transform.position = GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().playerLocation;
            GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().playerScene = Application.loadedLevelName;
        }
    }
}
