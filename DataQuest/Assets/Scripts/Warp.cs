using UnityEngine;
using System.Collections;

public class Warp : MonoBehaviour 
{
	public Transform WarpTarget;
	
	IEnumerator OnTriggerEnter2D(Collider2D other)
	{
		Debug.Log("Warp Triggered");
		//als de player over de colliders loopt word hij naar de warp target  gestuurd 
		//het zelfde geld voor de camera.
		ScreenFader sf = GameObject.FindGameObjectWithTag ("Fader").GetComponent<ScreenFader>();

		yield return StartCoroutine (sf.FadeToBlack());
		
		other.gameObject.transform.position = WarpTarget.position;
		Camera.main.transform.position = WarpTarget.position;

		yield return StartCoroutine (sf.FadeToClear());
	}
}
