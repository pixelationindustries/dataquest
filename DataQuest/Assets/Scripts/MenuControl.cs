﻿using UnityEngine;
using System;
using System.Collections;

public class MenuControl : MonoBehaviour {

    private GameObject Canvas;

    // Use this for initialization
    void Start() {
        Canvas = gameObject.GetComponentInChildren<Canvas>().gameObject;
        Canvas.SetActive(false);
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetButtonDown("Cancel") && Canvas.activeSelf == true) {
            Canvas.SetActive(false);
        }
        else if (Input.GetButtonDown("Cancel") && Canvas.activeSelf == false) {
            Canvas.SetActive(true);
        }
    }

    public void CloseMenu() {
        Canvas.SetActive(false);
    }

    public void CloseGame() {
        Application.Quit();
    }

    public void SaveGame() {
        PlayerPrefs.DeleteAll();

        GameObject Player = GameObject.Find("Player");
        SaveData saveData = GameObject.Find("Savedata(Clone)").GetComponent<SaveData>();

        PlayerPrefs.SetFloat("locX", Player.transform.position.x);
        PlayerPrefs.SetFloat("locY", Player.transform.position.y);

        PlayerPrefs.SetString("playerScene", Application.loadedLevelName);
        print(PlayerPrefs.GetString("playerScene"));

        PlayerPrefs.SetInt("beatBoss1", Convert.ToInt16(saveData.beatBoss1));
        PlayerPrefs.SetInt("beatBoss2", Convert.ToInt16(saveData.beatBoss2));
        PlayerPrefs.SetInt("beatBoss3", Convert.ToInt16(saveData.beatBoss3));
        PlayerPrefs.SetInt("beatBoss4", Convert.ToInt16(saveData.beatBoss4));

    }
}
