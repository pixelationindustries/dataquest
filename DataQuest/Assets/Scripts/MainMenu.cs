﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
    public GameObject savedata;
    private GameObject Canvas;

    // Use this for initialization
    void Start() {
        if (!PlayerPrefs.HasKey("playerScene")) {
            GameObject.Find("ButtonContinue").GetComponent<Button>().interactable = false;
        }

        Canvas = gameObject.GetComponentInChildren<Canvas>().gameObject;
    }

    public void CloseMenu() {
        Canvas.SetActive(false);
    }

    public void CloseGame() {
        Application.Quit();
    }

    public void NewGame() {
        Application.LoadLevel("Intro");
    }

    public void LoadGame() {
        Instantiate(savedata);
        SaveData saveData = GameObject.Find("Savedata(Clone)").GetComponent<SaveData>();

        saveData.playerLocation = new Vector3(PlayerPrefs.GetFloat("locX"), PlayerPrefs.GetFloat("locY"));
        saveData.playerScene = PlayerPrefs.GetString("playerScene");
        saveData.beatBoss1 = Convert.ToBoolean(PlayerPrefs.GetInt("beatBoss1"));
        saveData.beatBoss2 = Convert.ToBoolean(PlayerPrefs.GetInt("beatBoss2"));
        saveData.beatBoss3 = Convert.ToBoolean(PlayerPrefs.GetInt("beatBoss3"));
        saveData.beatBoss4 = Convert.ToBoolean(PlayerPrefs.GetInt("beatBoss4"));

        Application.LoadLevel(saveData.playerScene);
        DontDestroyOnLoad(GameObject.Find("Savedata(Clone)"));
    }
}
