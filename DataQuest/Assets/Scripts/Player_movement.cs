using UnityEngine;
using System.Collections;

public class Player_Movement : MonoBehaviour {

	Rigidbody2D Rbody;
	SpriteRenderer SRenderer;
	public Sprite characterLeft;
	public Sprite characterRight;
	public Sprite characterFront;
	public Sprite characterBack;

	public float walkSpeed;
//
//	//Animator Anim;
//
//	// hier wordt geinitialiseerd
	void Start () 
	{
//		//hier wordt toegang gegeven voor de componenten die aan de speler zijn gekoppeld
		Rbody = GetComponent<Rigidbody2D> ();
		SRenderer = GetComponent<SpriteRenderer>();
//		//Anim = GetComponent<Animator> ();
	}
//	
//	// Update word per frame opnieuw aangeroepen
//	void Update () 
//	{
//		//Maak een nieuwe vector aan 
//		//De x en y worden de horizon en verticale as, Deze worden gebruikt in de controles bij Unity
//		//GetAxisRaw geeft net als een Booliean True or False terug, ook zit er geen delay in
//		//Als je links klikt wordt horizontaal -1, laat je deze los dan wordt horizontaal 0.
		
//
//		// als movement_vector niet nul is moet je bewegen
////		if(movement_vector != Vector2.zero)
////		{//als je loopt wordt de movement_vector op true gezet
////			Anim.SetBool("Walking", true);
////		 // lever de movement vectors naar de animatie
////			Anim.SetFloat("Input_x", movement_vector.x);
////			Anim.SetFloat("Input_y", movement_vector.y);
////		// de variable worden alleen ge-update als er op de toetsen wordt gedrukt
////		}
////		else
////		{//als je niet loopt wordt de movement_vector op false gezet
////			Anim.SetBool("Walking", false);
////		}
//		//movement meegeven aan de player
//
//		Debug.Log(Rbody.velocity);

//		
//		Rbody.MovePosition(Rbody.position + movement_vector * Time.deltaTime);
//		// * Time.deltaTime houd in dat je ipv 1 meter per frame gaat je meer dan 1meter per frame gaat
//	}  


	void FixedUpdate()
	{
		Vector2 movement_vector = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

		// Move senteces
		Rbody.velocity = new Vector2(Mathf.Lerp(0, Input.GetAxis("Horizontal")* walkSpeed, 0.8f),
		                                                   Mathf.Lerp(0, Input.GetAxis("Vertical")* walkSpeed, 0.8f));

		if (movement_vector.x > 0){
			SRenderer.sprite = characterRight;
		}
		if (movement_vector.x < 0){
			SRenderer.sprite = characterLeft;
		}
		if (movement_vector.y > 0){
			SRenderer.sprite = characterBack;
		}
		if (movement_vector.y < 0){
			SRenderer.sprite = characterFront;
		}
    }
}
