using UnityEngine;
using System.Collections;

public class Camera_Follow : MonoBehaviour {

	//transform = slaat positie ,groote en de schaal op
	public Transform target;
	public float camera_distance = 25f;
	public float m_speed = 0.1f;

	//Camera verwijst naar de camera die wordt gebruikt in de game
	Camera mycam;

	// Use this for initialization
	void Start () 
	{
		mycam = GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void Update () 
	{//hierdoor wordt gezorgt dat het speel scherm word aangepast aan de resolutie waarmee je speelt

		mycam.orthographicSize = (Screen.height / camera_distance) / 4f;

		if (target)
		{
			transform.position = Vector3.Lerp(transform.position, target.position , m_speed ) +new Vector3 (0, 0, -20);
		}
	}
}
