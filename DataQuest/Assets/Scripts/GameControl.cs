﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameControl : MonoBehaviour {

	public static GameControl control;

	public float health;
	public float experience;
	
	void Awake () {
		if (control == null) {
			DontDestroyOnLoad (gameObject);
			control = this;
		} else if (control != this) {
			Destroy (gameObject);
		}
	}

	void OnGUI () {
		GUI.Label (new Rect (10, 10, 200, 30), "Health: " + health);
		GUI.Label (new Rect (10, 40, 200, 30), "Experience: " + experience);
	}

	//public void Save(int slot)
	public void Save()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/saveGame1.sav");

		SaveGame data = new SaveGame ();
		data.health = health;
		data.experience = experience;

		bf.Serialize (file, data);
		file.Close ();
	}

	public void Load()
	{
		if (File.Exists (Application.persistentDataPath + "/saveGame1.sav")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/saveGame1.sav", FileMode.Open);
			SaveGame data = (SaveGame)bf.Deserialize (file);
			file.Close ();

			health = data.health;
			experience = data.experience;
		}
	}
}

[Serializable]
class SaveGame
{
	public float health;
	public float experience;
}