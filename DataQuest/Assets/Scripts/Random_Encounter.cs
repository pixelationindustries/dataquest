﻿using UnityEngine;
using System.Collections;

public class Random_Encounter : MonoBehaviour {

    public string warpTarget;
	Vector3 PrevPosition;
	Vector3 CurrPosition;
	float EncounterChance;
	bool BattleStarted = false;


	void Start () 
	{
		PrevPosition = GetComponent <Transform> ().position;
		CurrPosition = PrevPosition;
	}
	
	void Update () 
	{
		CurrPosition = GetComponent <Transform>().position;

		if(PrevPosition != CurrPosition)
		{
			EncounterChance += Random.Range(0f,0.005f);

			float RandomValue = Random.Range (0f,100f);

			if(RandomValue < EncounterChance)
			{

				Debug.Log("Battle Started "+ EncounterChance);
				BattleStarted = true;

				StartBattle();
				EncounterChance = 0;

			}
			Debug.Log ("Encounter Chance " + EncounterChance +"\tRandom Value "+ RandomValue );
		}

		PrevPosition = CurrPosition;
	
	}

	public void StartBattle(){
        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().playerLocation = GameObject.Find("Player").transform.position;

        Application.LoadLevel(warpTarget);
        DontDestroyOnLoad(GameObject.Find("Savedata(Clone)"));
	}
}
