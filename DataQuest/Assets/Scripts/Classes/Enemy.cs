﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public string Name;
	public float MaxHealth;
	public float Health;
	public float PhysicalDamage;
	public float MagicDamage;
	public float PhysicalDefense;
	public float MagicDefense;
	public int[] Drops;
	public float Gold;

	public Sprite EnemySprite;
}
