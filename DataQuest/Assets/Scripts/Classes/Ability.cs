﻿using UnityEngine;
using System.Collections;

public class Ability : MonoBehaviour {

    public int AbilityId;

	public string Name;
    public string Description;

    public float ManaCost;
    public float PhypsicalBonus;
    public float MagicalBonus;
}
