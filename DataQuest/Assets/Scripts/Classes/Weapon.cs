﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	int ItemId;
	Sprite ItemSprite;

	WeaponType Type;

	string Name;

	string Description;
	float Value;

	float PhysicalDamage;
	float MagicDamage;

	float CritDamage;
	float CritChance;

	float Droprate;

}

public enum WeaponType{Sword, Hammer, Spear, Bow, Shuriken, Staff, Wand, Orb}
