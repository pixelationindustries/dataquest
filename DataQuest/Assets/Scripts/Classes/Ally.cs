﻿using UnityEngine;
using System.Collections;

public class Ally : MonoBehaviour {

	public string Name;
	public Sprite CharacterSprite;

	public float MaxHealth;
	public float Health;
    public float MaxMana;
    public float Mana;
	public float PhysicalDefense;
	public float MagicDefense;

	public float PhysicalDamage;
	public float MagicDamage;

	public float CritDamage;
	public float CritChance;

    public int[] AbilityIds;

	public int HelmetId;
	public int ChestId;
	public int PantsId;
	public int WeaponId;
}
