﻿using UnityEngine;
using System.Collections;

public class Warp_Scene : MonoBehaviour {

    public string target;
    public Vector3 targetLocation;

    void OnTriggerEnter2D() {
        //GameObject.Find("Savedata").GetComponent<SaveData>().playerLocation = GameObject.Find("Player").transform.position;
        WarpToTarget();
    }

    public void WarpToTarget() {
        if (GameObject.Find("Savedata(Clone)") != null) {
            GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().playerLocation = targetLocation;

            print(target);
            Application.LoadLevel(target);

            if (target != "MainMenu")
            {
                DontDestroyOnLoad(GameObject.Find("Savedata(Clone)"));
            }
        }
        else {
            Application.LoadLevel(target);
        }

    }
}
