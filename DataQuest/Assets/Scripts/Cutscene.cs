﻿using UnityEngine;
using System.Collections;

public class Cutscene : MonoBehaviour
{

    public GameObject camera;
    public int speed = 1;
    public float time = 100;
    public bool moving = true;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Skip") != 0)
        {
            GetComponent<Warp_Scene>().WarpToTarget();
        }
        else if(moving){
            camera.transform.Translate(Vector3.down * Time.deltaTime * speed);

            StartCoroutine(waitFor());
        }
    }

    IEnumerator waitFor()
    {
        yield return new WaitForSeconds(time);
        GetComponent<Warp_Scene>().WarpToTarget();
    }
}