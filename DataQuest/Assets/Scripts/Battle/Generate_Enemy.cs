﻿using UnityEngine;

public class Generate_Enemy : MonoBehaviour
{

	Enemy enemy;

	// Use this for initialization
	void Start ()
	{
		enemy = GetComponent<Enemy> ();

		GetComponent<SpriteRenderer> ().sprite = enemy.EnemySprite;
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
