﻿using UnityEngine;
using System.Collections;

public class Flee : MonoBehaviour
{
	BattleText BattleText;
	GameObject[] Allies;
	public Sprite runSprite;
	ScreenFader fader;

	bool FirstUpdate = true;
	float startPosition;

	// Use this for initialization
	void Start ()
	{
		BattleText = GameObject.Find("Battle Text").GetComponent<BattleText>(); 

		Allies = GameObject.FindGameObjectsWithTag ("Ally");

		BattleText.ShowText("The party ran away!");
		//fader = GameObject.Find("Fader").GetComponent<ScreenFader>();
		//if (fader == null){
		//	Debug.Log("Geen Fader!");
		//}

		//FadeOut();
	}
	// Update is called once per frame
	void Update ()
	{
		foreach (GameObject ally in Allies) {
			float runSpeed = Random.Range(2f, 5f);

			Vector3 startPos = ally.transform.position;
			Vector3 endPos = new Vector3(ally.transform.position.x - runSpeed, ally.transform.position.y);

			ally.transform.position = Vector3.Lerp (startPos, endPos, Time.deltaTime);

			ally.GetComponent<SpriteRenderer>().sprite = runSprite;
			if (FirstUpdate){

				FirstUpdate = false;
				startPosition = transform.position.x;


			}

			//Debug.Log("Startpos: "+ startPos + "startPosition: "+ (startPosition - 10f));
			if (startPos.x < startPosition - 15f){
				Application.LoadLevel("testClass");
			}
		}
	}

	IEnumerator FadeOut(){
		yield return StartCoroutine(fader.FadeToBlack());
	}
}
