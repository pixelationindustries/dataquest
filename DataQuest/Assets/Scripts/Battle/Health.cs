﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using System;

public class Health : MonoBehaviour {

    public float maxHealth;
    public float currentHealth;

    public float maxMana;
    public float currentMana;

    bool runFirstTimeHp = true;
    bool runFirstTimeMp = true;

    // Use this for initialization
    void Start() {

        if (this.gameObject.transform.parent.parent.tag == "Enemy") {
            maxHealth = GetComponentInParent<Enemy>().MaxHealth;
            currentHealth = GetComponentInParent<Enemy>().Health;
        }
        if (this.gameObject.transform.parent.parent.tag == "Ally") {
            maxHealth = GetComponentInParent<Ally>().MaxHealth;
            currentHealth = GetComponentInParent<Ally>().Health;
            maxMana = GetComponentInParent<Ally>().MaxMana;
            currentMana = GetComponentInParent<Ally>().Mana;
        }

    }

    public void TakeDamage(float damage) {
        //ally
        if (this.gameObject.transform.parent.parent.tag.Contains("Ally")) {
            LoseHealth(1, damage);
            LoseHealth(2, damage);
            LoseHealth(3, damage);
        }

        //enemy
        if (this.transform.parent.parent.gameObject.tag == "Enemy") {

            currentHealth = currentHealth - damage;
            GetComponentInParent<Enemy>().Health = currentHealth;
            GetComponent<Transform>().localScale = new Vector3((currentHealth / maxHealth) * 100, 10, 1);

            if (currentHealth <= 0) {
                Destroy(transform.parent.parent.gameObject);
                Battle_Controller.EnemyCount--;
            }

            //Battle_Controller.SetMoved(true);
        }
    }

    public void ChangeMana(float changeAmount) {
        if (this.gameObject.transform.parent.parent.tag.Contains("Ally")) {
            LoseMana(1, changeAmount);
            LoseMana(2, changeAmount);
            LoseMana(3, changeAmount);
        }
    }

    void LoseHealth(int targetNumber, float damage) {
        if (this.gameObject.transform.parent.parent.name.Contains(Convert.ToString(targetNumber))) {

            //GameObject hpBar = GameObject.Find("Character Panel " + Convert.ToString(targetNumber)).GetComponentInChildren<Health>().gameObject;
            GameObject hpBar = GameObject.Find("HP Bar " + Convert.ToString(targetNumber)).GetComponent<Health>().gameObject;

            currentHealth = currentHealth - damage;
            GetComponentInParent<Ally>().Health = currentHealth;
            GetComponent<Transform>().localScale = new Vector3((currentHealth / maxHealth) * 100, 10, 1);

            SyncBar("Health", hpBar, damage, targetNumber);

            if (currentHealth <= 0) {
                Destroy(transform.parent.parent.gameObject);
                Battle_Controller.AllyCount--;
            }
        }
    }

    void LoseMana(int targetNumber, float amount) {
        if (this.gameObject.transform.parent.parent.name.Contains(Convert.ToString(targetNumber))) {

            //GameObject mpBar = GameObject.Find("Character Panel " + Convert.ToString(targetNumber)).GetComponentInChildren<Health>().gameObject;
            GameObject mpBar = GameObject.Find("MP Bar " + Convert.ToString(targetNumber)).GetComponent<Health>().gameObject;

            currentMana = currentMana - amount;
            GetComponentInParent<Ally>().Mana = currentMana;
            GetComponent<Transform>().localScale = new Vector3((currentMana / maxMana) * 100, 10, 1);

            SyncBar("Mana", mpBar, amount, targetNumber);
        }
    }

    public void SyncBar(string barType, GameObject target, float amount, int targetNumber) {
        if (barType == "Health") {
            if (runFirstTimeHp) {

                if (currentHealth != maxHealth) {
                    amount = maxHealth - currentHealth;
                }

                runFirstTimeHp = false;
            }

            GameObject bar = target;

            float lostPercentage = amount / maxHealth * 100;
            float lostBar = lostPercentage / 22;

            bar.transform.parent.parent.GetComponentsInChildren<Text>()[1].text = Convert.ToString(currentHealth + " | " + maxHealth);

            Vector3 newPosition = new Vector3(bar.transform.position.x - (lostBar), bar.transform.parent.position.y);
            bar.transform.position = newPosition;
        }

        if (barType == "Mana") {
            if (runFirstTimeMp) {

                if (currentMana != maxMana) {
                    amount = maxMana - currentMana;
                }

                runFirstTimeMp = false;
            }

            GameObject bar = target;

            float lostPercentage = amount / maxMana * 100;
            float lostBar = lostPercentage / 22;

            bar.transform.parent.parent.GetComponentsInChildren<Text>()[3].text = Convert.ToString(currentMana + " | " + maxMana);

            Vector3 newPosition = new Vector3(bar.transform.position.x - (lostBar), bar.transform.parent.position.y);
            bar.transform.position = newPosition;
        }
    }
}
