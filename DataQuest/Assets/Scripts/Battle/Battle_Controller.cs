using UnityEngine;
using System;
using UnityEngine.UI;

public class Battle_Controller : MonoBehaviour {

    public enum BattleStates {
        START,
        ALLY1_CHOICE,
        ENEMY1_CHOICE,
        ALLY2_CHOICE,
        ENEMY2_CHOICE,
        ALLY3_CHOICE,
        ENEMY3_CHOICE,
        LOSE,
        WIN
    }

    private bool toggleControls = true;
    public string returnTarget;
    public static int pickedTarget;
    public static int AllyCount;
    public static int EnemyCount;
    private BattleStates currentState;
    public static bool ControlsEnabled = false;
    private GameObject ControlCover;
    private GameObject[] Allies = new GameObject[3];
    public static GameObject[] AllyHUDs = new GameObject[3];
    public static GameObject[] EnemyHUDs = new GameObject[3];
    public static BattleText BattleText;
    private bool firstTime;
    private bool firstTimeMove;
    private bool wentToTarget;
    private GameObject[] allies;
    private GameObject[] enemies = new GameObject[3];
    private GameObject target;
    private Vector3 startPosition;
    private Vector3 currentPosition;
    private Vector3 endPosition;
    int attackType;
    private Ability[] Abilities = new Ability[3];

    // Use this for initialization
    private void Start() {
        Abilities[0] = GetComponentsInParent<Ability>()[0];
        Abilities[1] = GetComponentsInParent<Ability>()[1];
        Abilities[2] = GetComponentsInParent<Ability>()[2];

        firstTime = true;
        firstTimeMove = true;

        AllyHUDs[0] = GameObject.Find("Ally HUD 1");
        AllyHUDs[1] = GameObject.Find("Ally HUD 2");
        AllyHUDs[2] = GameObject.Find("Ally HUD 3");

        EnemyHUDs[0] = GameObject.Find("Enemy HUD 1");
        EnemyHUDs[1] = GameObject.Find("Enemy HUD 2");
        EnemyHUDs[2] = GameObject.Find("Enemy HUD 3");

        if (GameObject.Find("Enemy 1"))
            enemies[0] = GameObject.Find("Enemy 1");

        if (GameObject.Find("Enemy 2"))
            enemies[1] = GameObject.Find("Enemy 2");

        if (GameObject.Find("Enemy 3"))
            enemies[2] = GameObject.Find("Enemy 3");


        Allies[0] = GameObject.Find("Ally 1");
        Allies[1] = GameObject.Find("Ally 2");
        Allies[2] = GameObject.Find("Ally 3");

        pickedTarget = 100;
        attackType = 0;

        foreach (GameObject HUD in AllyHUDs) {
            HUD.transform.localScale = new Vector3(0, 0, 1);
        }
        foreach (GameObject HUD in EnemyHUDs) {
            HUD.transform.localScale = new Vector3(0, 0, 1);
        }

        AllyCount = GameObject.FindGameObjectsWithTag("Ally").Length;
        EnemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;
        Debug.Log("Allies: " + AllyCount + " Enemies: " + EnemyCount);

        BattleText = GameObject.Find("Battle Text").GetComponent<BattleText>();

        currentState = BattleStates.START;
    }

    // Update is called once per frame
    private void Update() {
        //Debug.Log (currentState);
        switch (currentState) {

            //Start State
            case (BattleStates.START):

                foreach (GameObject ally in Allies) {
                    ally.GetComponentInChildren<Health>().TakeDamage(0);
                    ally.GetComponentInChildren<Health>().ChangeMana(0);
                }

                NextState();
                break;

            //Turns
            case (BattleStates.ALLY1_CHOICE):
                if (Allies[0].gameObject != null && Allies[0].GetComponentInChildren<Health>().currentHealth > 0) {

                    CheckMana(0);
                    if (toggleControls) {
                        SwitchControls(true);
                        toggleControls = false;
                    }

                    PlayerMove();

                }
                else {
                    NextState();
                }

                break;
            case (BattleStates.ENEMY1_CHOICE):
                toggleControls = true;
                try {
                    EnemyMove(GameObject.Find("Enemy 1").GetComponent<Enemy>());
                }
                catch {
                    Debug.Log("Enemy does not exist!");
                    NextState();
                }

                break;

            case (BattleStates.ALLY2_CHOICE):
                if (Allies[1].gameObject != null && Allies[1].GetComponentInChildren<Health>().currentHealth > 0) {

                    CheckMana(1);
                    if (toggleControls) {
                        SwitchControls(true);
                        toggleControls = false;
                    }

                    PlayerMove();
                }
                else {
                    NextState();
                }

                break;
            case (BattleStates.ENEMY2_CHOICE):
                toggleControls = true;
                try {
                    EnemyMove(GameObject.Find("Enemy 2").GetComponent<Enemy>());
                }
                catch {
                    Debug.Log("Enemy does not exist!");
                    NextState();
                }

                break;

            case (BattleStates.ALLY3_CHOICE):
                if (Allies[2].gameObject != null && Allies[2].GetComponentInChildren<Health>().currentHealth > 0) {

                    CheckMana(2);

                    if (toggleControls) {
                        SwitchControls(true);
                        toggleControls = false;
                    }

                    PlayerMove();

                }
                else {
                    NextState();
                }

                break;

            case (BattleStates.ENEMY3_CHOICE):

                toggleControls = true;
                try {
                    EnemyMove(GameObject.Find("Enemy 3").GetComponent<Enemy>());
                }
                catch {
                    Debug.Log("Enemy does not exist!");
                    NextState();
                }

                break;

            case (BattleStates.LOSE):

                foreach (GameObject ally in GameObject.FindGameObjectsWithTag("Ally")) {
                    if (ally != null && ally.GetComponentInChildren<Health>().currentMana <= 75) {
                        ally.GetComponentInChildren<Health>().ChangeMana(-25);
                    }
                    else {
                        ally.GetComponentInChildren<Health>().ChangeMana(-(ally.GetComponentInChildren<Health>().maxMana - ally.GetComponentInChildren<Health>().currentMana));
                    }
                }

                if (AllyCount < 1) {
                    Debug.Log("Player Loses");
                    Application.LoadLevel("GameOver");
                }
                NextState();
                break;

            case (BattleStates.WIN):
                if (EnemyCount < 1) {
                    Debug.Log("Player Wins");

                    Warp_Scene warp = gameObject.GetComponent<Warp_Scene>();

                    if (Application.loadedLevelName.Contains("Battle")) {
                        Application.LoadLevel(returnTarget);
                    }

                    if (Application.loadedLevelName == "Level1Boss") {
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss1 = true;
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss2 = false;
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss3 = false;
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss4 = false;
                        warp.WarpToTarget();
                    }
                    if (Application.loadedLevelName == "Level2Boss") {
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss2 = true;
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss1 = false;
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss3 = false;
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss4 = false;
                        warp.WarpToTarget();
                    }
                    if (Application.loadedLevelName == "Level3Boss") {
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss3 = true;
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss1 = false;
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss2 = false;
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss4 = false;
                        warp.WarpToTarget();
                    }
                    if (Application.loadedLevelName == "Level4Boss") {
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss4 = true;
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss1 = false;
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss2 = false;
                        GameObject.Find("Savedata(Clone)").GetComponent<SaveData>().beatBoss3 = false;
                        warp.WarpToTarget();
                    }


                    DontDestroyOnLoad(GameObject.Find("Savedata(Clone)"));
                }
                NextState();
                break;
        }


    }

    void CheckMana(int ally) {

        if (Allies[ally].GetComponent<Ally>().Mana < Abilities[0].ManaCost) {
            GameObject[] buttons = GameObject.FindGameObjectsWithTag("Ability 1 Button");
            foreach (GameObject button in buttons) {
                button.GetComponent<Button>().interactable = false;
            }
        }
        else {
            GameObject[] buttons = GameObject.FindGameObjectsWithTag("Ability 1 Button");
            foreach (GameObject button in buttons) {
                button.GetComponent<Button>().interactable = true;
            }
        }

        if (Allies[ally].GetComponent<Ally>().Mana < Abilities[1].ManaCost) {
            GameObject[] buttons = GameObject.FindGameObjectsWithTag("Ability 2 Button");
            foreach (GameObject button in buttons) {
                button.GetComponent<Button>().interactable = false;
            }
        }
        else {
            GameObject[] buttons = GameObject.FindGameObjectsWithTag("Ability 2 Button");
            foreach (GameObject button in buttons) {
                button.GetComponent<Button>().interactable = true;
            }
        }

        if (Allies[ally].GetComponent<Ally>().Mana < Abilities[2].ManaCost) {
            GameObject[] buttons = GameObject.FindGameObjectsWithTag("Ability 3 Button");
            foreach (GameObject button in buttons) {
                button.GetComponent<Button>().interactable = false;
            }
        }
        else {
            GameObject[] buttons = GameObject.FindGameObjectsWithTag("Ability 3 Button");
            foreach (GameObject button in buttons) {
                button.GetComponent<Button>().interactable = true;
            }
        }
    }

    public void NextState() {
        firstTime = true;
        currentState++;

        if (EnemyCount < 1) {
            currentState = BattleStates.WIN;
        }
        if (AllyCount < 1) {
            currentState = BattleStates.LOSE;
        }

        if (currentState == (BattleStates)9) {
            currentState = BattleStates.ALLY1_CHOICE;
        }
    }

    private void EnemyMove(Enemy enemy) {
        if (firstTime) {
            allies = GameObject.FindGameObjectsWithTag("Ally");
            target = allies[UnityEngine.Random.Range(0, allies.Length)];

            startPosition = enemy.transform.position;
            currentPosition = enemy.transform.position;
            endPosition = target.transform.position;

            firstTime = false;
            wentToTarget = false;

        }

        if (enemy != null) {

            // Move to target
            if (Vector3.Distance(currentPosition, endPosition) > 1f && !wentToTarget) {
                currentPosition = Vector3.MoveTowards(currentPosition, endPosition, 6f * Time.deltaTime);
                enemy.transform.position = currentPosition;

                //Attack target
            }
            else if (!wentToTarget) {
                wentToTarget = true;

                if (target == null) {
                    currentState = BattleStates.LOSE;
                    return;
                }
                float damage = CalculateDamage(11, enemy.gameObject, target.gameObject);

                EnemyAttack(target.gameObject, damage);

                BattleText.ShowText(String.Format("<color=maroon>{0}</color> hit <color=#008000ff>{1}</color> for <color=red>{2}</color> damage", enemy.GetComponent<Enemy>().Name, target.GetComponent<Ally>().Name, damage));
                Debug.Log(enemy.GetComponent<Enemy>() + " Attacked " + target.GetComponent<Ally>());
            }

            // Move back to starting position
            if (currentPosition != startPosition && wentToTarget) {
                currentPosition = Vector3.MoveTowards(currentPosition, startPosition, 8f * Time.deltaTime);
                enemy.transform.position = currentPosition;
            }
            else if (enemy.transform.position == startPosition && wentToTarget) {
                NextState();
            }
        }
        else {
            Debug.Log("Enemy doesn't exist!");
            NextState();
        }
    }

    private void EnemyAttack(GameObject target, float damage) {
        if (target == null) {
            currentState = BattleStates.LOSE;
            return;
        }
        target.GetComponentInChildren<Health>().TakeDamage(damage);
    }

    public void SwitchControls(bool value) {

        if (value) {
            switch ((int)currentState) {
                case (1):
                    AllyHUDs[0].transform.localScale = new Vector3(1, 1, 1);

                    break;
                case (3):
                    AllyHUDs[1].transform.localScale = new Vector3(1, 1, 1);
                    break;
                case (5):
                    AllyHUDs[2].transform.localScale = new Vector3(1, 1, 1);
                    break;
            }

            ControlsEnabled = true;
            return;
        }

        switch ((int)currentState) {
            case (1):
                AllyHUDs[0].transform.localScale = new Vector3(0, 0, 1);
                break;
            case (3):
                AllyHUDs[1].transform.localScale = new Vector3(0, 0, 1);
                break;
            case (5):
                AllyHUDs[2].transform.localScale = new Vector3(0, 0, 1);
                break;
        }
        foreach (GameObject enemyHuD in EnemyHUDs) {
            enemyHuD.transform.localScale = new Vector3(0, 0, 0);
        }

        ControlsEnabled = false;
    }

    public void Attack(int AttackType) {
        SwitchControls(false);
        attackType = AttackType;
        print(AttackType + " " + pickedTarget);
    }

    private void PlayerMove() {
        if (pickedTarget != 100 && attackType != 0) {
            if (firstTime) {
                SwitchControls(false);
                firstTime = false;
                foreach (GameObject hud in Battle_Controller.EnemyHUDs) {
                    hud.transform.localScale = new Vector3(0, 0, 1);
                }
            }

            if (currentState == BattleStates.ALLY1_CHOICE) {
                PerformAttack(Allies[0], enemies[pickedTarget]);
            }
            else if (currentState == BattleStates.ALLY2_CHOICE) {
                PerformAttack(Allies[1], enemies[pickedTarget]);
            }
            else if (currentState == BattleStates.ALLY3_CHOICE) {
                PerformAttack(Allies[2], enemies[pickedTarget]);
            }
        }
    }

    private void PerformAttack(GameObject start, GameObject end) {
        if (firstTimeMove) {
            print(start.name + " " + end.name);

            startPosition = start.transform.position;
            currentPosition = start.transform.position;
            endPosition = end.transform.position;

            firstTimeMove = false;
            wentToTarget = false;

        }

        // Move to target
        if (Vector3.Distance(currentPosition, endPosition) > 1f && !wentToTarget) {
            currentPosition = Vector3.MoveTowards(currentPosition, endPosition, 6f * Time.deltaTime);
            start.transform.position = currentPosition;
        }
        else if (!wentToTarget) {
            wentToTarget = true;
            float damage = CalculateDamage(attackType, start, end);

            //send attack here
            GameObject.Find(String.Format("Enemy " + (pickedTarget + 1))).GetComponentInChildren<Health>().TakeDamage(damage);

            BattleText.ShowText(String.Format("<color=#008000ff>{0}</color> hit <color=maroon>{1}</color> for <color=red>{2}</color> damage", start.GetComponent<Ally>().Name, end.GetComponent<Enemy>().Name, damage));
        }

        // Move back to starting position
        if (currentPosition != startPosition && wentToTarget) {
            currentPosition = Vector3.MoveTowards(currentPosition, startPosition, 8f * Time.deltaTime);
            start.transform.position = currentPosition;
        }
        else if (start.transform.position == startPosition && wentToTarget) {
            start = null;
            end = null;
            firstTimeMove = true;
            SwitchControls(false);
            pickedTarget = 100;
            attackType = 0;
            NextState();
        }
    }

    private int CalculateDamage(int attackType, GameObject attacker, GameObject target) {
        float damage = 0;
        Ability ability = null;

        if (attackType == 10) {
            print("allydamage: " + attacker.GetComponent<Ally>().PhysicalDamage + ", enemyDefense: " + target.GetComponent<Enemy>().PhysicalDefense);
            damage = attacker.GetComponent<Ally>().PhysicalDamage * (1 - target.GetComponent<Enemy>().PhysicalDefense);
        }
        else if (attacker.GetComponent<Ally>() != null) {
            //ally attacks

            if (attackType == 1) {
                ability = Abilities[0];
            }
            else if (attackType == 2) {
                ability = Abilities[1];
            }
            else if (attackType == 3) {
                ability = Abilities[2];
            }
            print("Mana cost: " + ability.ManaCost);

            if (attacker.GetComponent<Ally>().Mana >= ability.ManaCost) {
                attacker.gameObject.GetComponentInChildren<Health>().ChangeMana(ability.ManaCost);

                float PhysicalDamage = (attacker.GetComponent<Ally>().PhysicalDamage * ability.PhypsicalBonus) *
                                       (1 - target.GetComponent<Enemy>().PhysicalDefense);
                float MagicalDamage = (attacker.GetComponent<Ally>().MagicDamage * ability.MagicalBonus) *
                                      (1 - target.GetComponent<Enemy>().MagicDefense);
                damage = PhysicalDamage + MagicalDamage;
            }
        }
        else {
            //enemy attacks
            float PhysicalDamage = attacker.GetComponent<Enemy>().PhysicalDamage * (1 - target.GetComponent<Ally>().PhysicalDefense);
            float MagicalDamage = attacker.GetComponent<Enemy>().MagicDamage * (1 - target.GetComponent<Ally>().MagicDefense);
            damage = PhysicalDamage + MagicalDamage;
        }

        print("calculatedDamage: " + damage);
        return (int)Math.Round(damage, 0);
    }
}