﻿using UnityEngine;
using System.Collections;

public class Attack : MonoBehaviour
{

	public float Damage;
	public bool ButtonPressed;

	public GameObject clickedObject;

	public void ButtonClick ()
	{

		ButtonPressed = true;
		Debug.Log ("Button pressed, pick enemy");
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetMouseButtonDown (0) && ButtonPressed) {

			clickedObject = GetClickedObject ();
			if (clickedObject != null && clickedObject.tag == "Enemy"){

				clickedObject.GetComponentInChildren<Health>().TakeDamage(Damage);
				//Debug.Log(clickedObject.GetComponent<Enemy>().Health);
			}


			ButtonPressed = false;
		}
	}

	GameObject GetClickedObject ()
	{
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
		if (hit) {
			//Debug.Log (hit.collider.gameObject.name);
			return hit.collider.gameObject;


		}
		return null;
	}
}
