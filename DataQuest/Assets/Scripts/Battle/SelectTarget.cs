﻿using UnityEngine;
using System;
using System.Collections;

public class SelectTarget : MonoBehaviour
{
	public GameObject clickedObject;
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetMouseButtonDown (0) && Battle_Controller.ControlsEnabled) {

			clickedObject = GetClickedObject ();
			if (clickedObject != null && clickedObject.tag == "Enemy") {

				foreach (GameObject hud in Battle_Controller.EnemyHUDs){
					hud.transform.localScale = new Vector3(0,0,1);
				}

				int selectedEnemy = Convert.ToInt16(clickedObject.name.Substring (clickedObject.name.Length -1, 1));
				Battle_Controller.pickedTarget = selectedEnemy-1;
				print(selectedEnemy -1 );

				Battle_Controller.EnemyHUDs [selectedEnemy-1].transform.localScale = new Vector3 (1, 1, 1);
			}

			//clickedObject.GetComponentInChildren<Health>().TakeDamage(25);
			//Debug.Log(clickedObject.GetComponent<Enemy>().Health);
		}
	}

	GameObject GetClickedObject ()
	{
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
		if (hit) {
			//Debug.Log (hit.collider.gameObject.name);
			return hit.collider.gameObject;
			
			
		}
		return null;
	}
}
