﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BattleText : MonoBehaviour {

    bool showText = false;
    bool endReached = false;

    Vector3 startPosition = Vector3.zero;
    Vector3 endPosition = Vector3.zero;

    public GameObject LogRow;

    float speed = 3;

    // Use this for initialization
    void Start() {
        startPosition = this.transform.position;
        endPosition = new Vector3(this.transform.position.x, this.transform.position.y - 1.5f);
    }

    // Update is called once per frame
    void Update() {
        if (showText) {
            if (Vector3.Distance(this.transform.position, endPosition) > 0.01f && !endReached) {
                this.transform.position = Vector3.Lerp(this.transform.position, endPosition, speed * Time.deltaTime);
            }
            else if (!endReached) {
                endReached = true;
            }

            if (this.transform.position != startPosition && endReached) {
                this.transform.position = Vector3.MoveTowards(this.transform.position, startPosition, speed * 2 * Time.deltaTime);
            }
            else if (this.transform.position == startPosition && endReached) {
                endReached = false;
                showText = false;
            }
        }
    }

    public void ShowText(string text) {

        GetComponentInChildren<Text>().text = text;
        UpdateBattleLog(text);

        showText = true;

    }

    IEnumerator Wait(float seconds) {
        yield return new WaitForSeconds(seconds);
    }

    private void UpdateBattleLog(string text) {
        GameObject row = Instantiate(LogRow);

        row.GetComponent<Text>().text = text;
        row.transform.SetParent(GameObject.Find("Log Row").transform.parent, false);
        GameObject.Instantiate(row);
    }
}