﻿using UnityEngine;
using System.Collections;

public class CheckProgress : MonoBehaviour {

    private SaveData savedata;

    // Use this for initialization
    void Start() {
        Debug.Log(GameObject.Find("Savedata(Clone)").GetComponent<SaveData>());
        savedata = GameObject.Find("Savedata(Clone)").GetComponent<SaveData>();

        if (savedata.beatBoss1 | savedata.beatBoss2 | savedata.beatBoss3 | savedata.beatBoss4)
        {
            GameObject.Find("Level1Barricade").SetActive(true);
        }
        else
        {
            print(GameObject.Find("level1Barricade"));
            GameObject.Find("Level1Barricade").SetActive(false);
        }

        if (savedata.beatBoss1)
        {
            GameObject.Find("Level2Barricade").SetActive(false);
        }

        if (savedata.beatBoss2)
        {
            GameObject.Find("Level3Barricade").SetActive(false);
        }

        if (savedata.beatBoss3)
        {
            GameObject.Find("Level4Barricade").SetActive(false);
        }

        if (savedata.beatBoss4)
        {
            Debug.Log("Game gewonnen, nog iets doen");
        }
    }

    // Update is called once per frame
    void Update() {

        
    }
}
